console.log(`Hello World`)

let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
let workAddress = { houseNumber: '32', street: 'Washington', city: 'Lincoln', state: 'Nebraska'};

function printUserInfo(firstName, lastName, age){
    console.log(`First Name: ${firstName}`);
    console.log(`Last Name: ${lastName}`);
    console.log(`Age: ${age}`);
    /* 
        Wanted to use backticks but had to use a different one in order to mimic expected output
        
        console.log(`Hobbies: ${hobbies}`);
        console.log(`Work Address: ${workAddress}`);
    */
    console.log('Hobbies: ', hobbies);
    console.log('Work Address: ', workAddress);
}

function returnValue(firstName, lastName, age) {
    return(`${firstName} ${lastName} is ${age} years of age.`);
}

printUserInfo('Raymond', 'De Castro', 30)

let user = returnValue('Raymond', 'De Castro', 30);

console.log(user);